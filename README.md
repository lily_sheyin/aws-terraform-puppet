### The Project
* Deploying a puppet provisioner and agent node(s) to host a java web app fronted by a secured nginx proxy.


### Setup

* First clone this repo.
* Next copy `tfvars.sample` to `tfvars` and edit it as per your setup.
* Run `terraform init -var-file=tfvars` to download the required modules.

### Invocation

`terraform plan -var-file tfvars`

`terraform apply -var-file tfvars`
