provider "aws" {
  profile = "${var.profile}"
  region  = "${var.region}"
}

module "vpc" {
  source     = "terraform-aws-modules/vpc/aws"

  name = "${replace(var.tag_owner, " ", "_")}_vpc"
  cidr = "10.4.0.0/16"
  azs             = ["${var.region}a"]
  public_subnets  = ["10.4.1.0/24"]

  enable_dns_hostnames = true
  enable_nat_gateway   = true

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

resource "aws_security_group" "sg-puppet-master" {
  name        = "Puppet"
  description = "Created by Terraform"
  vpc_id      = "${module.vpc.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.myip}/32"]
    description = "SSH"
  }
  ingress {
    from_port   = 8140
    to_port     = 8140
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Puppet"
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  tags {
    Name = "${var.tag_owner} || puppet-master"
  }
}

resource "aws_security_group" "sg-puppet-agent" {
  name        = "PuppetAgent"
  description = "Created by Terraform"
  vpc_id      = "${module.vpc.vpc_id}"

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks     = ["${var.myip}/32"]
    description     = "SSH"
  }
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Webapp"
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Http"
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Https"
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  tags {
    Name = "${var.tag_owner} || puppet-agent"
  }
}
