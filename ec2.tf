data "template_file" "site" {
  template = "${file("configs/site.pp.tpl")}"

  vars {
    region = "${var.region}"
  }
}

resource "aws_instance" "puppet-master" {
  ami                    = "ami-2a7d75c0"
  instance_type          = "t2.micro"
  key_name               = "${aws_key_pair.sshkey.key_name}"
  subnet_id              = "${module.vpc.public_subnets[0]}"
  vpc_security_group_ids = ["${aws_security_group.sg-puppet-master.id}"]
  user_data              = "${file("userdata/cloud-init-master.conf")}"

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = "${file("${var.ssh_private_key_path}")}"
  }

  provisioner "file" {
    source      = "webapp"
    destination = "/tmp"
  }

  provisioner "file" {
    source      = "configs"
    destination = "/tmp"
  }

  provisioner "remote-exec" {
    inline = [
      "while [ ! -f /tmp/signal ]; do sleep 2; done",
      "echo 'agent1.${var.region}.compute.internal' | sudo tee /etc/puppet/autosign.conf",
      "sudo mv /tmp/webapp/* /etc/puppet/modules/weatherapp/files",
      "sudo mv /tmp/configs/init.pp /etc/puppet/modules/weatherapp/manifests/init.pp",
      "echo '${data.template_file.site.rendered}' | sudo tee /etc/puppet/manifests/site.pp"
    ]
  }

  tags {
    Name = "${var.tag_owner} || puppet-master"
  }
}

resource "aws_instance" "puppet-agent" {
  ami                    = "ami-2a7d75c0"
  instance_type          = "t2.micro"
  key_name               = "${aws_key_pair.sshkey.key_name}"
  subnet_id              = "${module.vpc.public_subnets[0]}"
  vpc_security_group_ids = ["${aws_security_group.sg-puppet-agent.id}"]
  user_data              = "${file("userdata/cloud-init-agent.conf")}"

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = "${file("${var.ssh_private_key_path}")}"
  }

  provisioner "remote-exec" {
    inline = [
      "while [ ! -f /tmp/signal ]; do sleep 2; done",
      "sudo sed -i '1a ${aws_instance.puppet-master.private_ip} puppet' /etc/hosts",
      "sudo systemctl stop puppet; sudo puppet agent --enable; sudo puppet agent --waitforcert=120"
    ]
  }

  tags {
    Name = "${var.tag_owner} || puppet-agent"
  }
}
