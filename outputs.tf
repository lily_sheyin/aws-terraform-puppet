output "Master IP" {
  value = "${aws_instance.puppet-master.public_ip}"
}

output "Agent IP" {
  value = "${aws_instance.puppet-agent.public_ip}"
}
