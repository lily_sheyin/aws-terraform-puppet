resource "aws_key_pair" "sshkey" {
  public_key = "${file("${var.ssh_public_key_path}")}"
}
