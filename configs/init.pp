class appfiles {
    package { "default-jre":
        before => Exec["start weather app"],
        ensure => 'installed',
    }

    file { "/opt/webapp":
        before => File["/opt/webapp/weather-app-0.1.0.jar"],
        ensure => directory,
        mode => '0755',
        owner => 'root',
        group => 'root',
    }

    file { "/opt/webapp/weather-app-0.1.0.jar":
        before => Exec["start weather app"],
        mode => "0644",
        owner => 'root',
        group => 'root',
        source => 'puppet:///modules/weatherapp/weather-app-0.1.0.jar',
    }

    file { "/opt/webapp/application.properties":
        before => Exec["start weather app"],
        mode => "0644",
        owner => 'root',
        group => 'root',
        source => 'puppet:///modules/weatherapp/application.properties',
    }

    file { "/opt/webapp/web.log":
        before => Exec["start weather app"],
        mode => "0644",
        owner => 'root',
        group => 'root',
        source => 'puppet:///modules/weatherapp/web.log',
    }

    exec { "start weather app":
        command => "nohup java -jar /opt/webapp/*.jar &",
        path => [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/' ],
    }
  }
