import "weatherapp"

node base {
  include appfiles
}

node "agent1.${region}.compute.internal" inherits base {
  class { "nginx": }
  file { "default-nginx-disable":
    ensure  => absent,
    path    => "/etc/nginx/conf.d/default.conf",
    require => Package['nginx'],
    notify  => Service['nginx'],
  }
  nginx::nginx_servers { "weatherapp.localhost":
    listen_options => "default_server",
    listen_port => 80,
    proxy       => "http://localhost:8080",
  }
}
