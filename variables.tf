variable "myip" {
  default = "77.108.144.180",
  description = "IP address for SSH access"
}

variable "tag_owner" {
  default     = "al"
  description = "First name of the owner (for tagging resources)"
}

variable "ssh_public_key_path" {
  default     = "~/.ssh/id_rsa.pub"
  description = "Path to your ssh PUBLIC key"
}

variable "ssh_private_key_path" {
  default     = "~/.ssh/id_rsa"
  description = "Path to your ssh PRIVATE key"
}

variable "region" {
  default = "eu-west-1"
}

variable "profile" {
  default     = "default"
  description = "Which set of AWS credentials to read. Taken from ~/.aws/credentials"
}
